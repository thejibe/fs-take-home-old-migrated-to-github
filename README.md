# JIBE FS Take-Home Exercise

💫 Welcome to the JIBE FS take-home coding exercise! 🎉

This fullstack exercise is designed to take about 4 hours for someone familiar with React/Node/Express. It involves building an API and storefront in Node.js & React to expose product and shopping cart functionality.

If you're finding that it's taking you longer than 4h to complete then please add a note with how you would approach any outstanding requirements you haven't been able to get to. 

## Getting Set Up

The exercise requires [Node.js](https://nodejs.org/en/) to be installed. We recommend using the LTS version.

## Quick Start

```bash
# Install dependencies
yarn (or npm install)

# Start development server
yarn dev (or npm run dev)
```

❗️ **Make sure you commit all changes to the master branch!**

## Technical Notes

- The server is running with [nodemon](https://nodemon.io/) which will automatically restart for you when you modify and save a file.
- The frontend was bootstrapped with [Create React App](https://facebook.github.io/create-react-app/docs/getting-started)
- Feel free to use what you think is best for the task given (routers, API frameworks, API clients, test frameworks, ..) 
- The server is running on port 5424 and all AJAX requests from the frontend will automatically proxy to that endpoint. For instance, you can `fetch('/api/username')` and it will automatically hit `localhost:5424/api/username`.

## Acceptance Criteria

Below is a list of user stories corresponding to our acceptance criteria.

1. A persistent storage layer (MySQL) is added to the backend using `docker`/`docker-compose`.

2. Product data in `src/seed.js` is seeded into the DB.

3. Product data is exposed through the API (either REST or GraphQL).

4. I can browse the product collection on a product listing page (PLP) with Category, Size and Color filters.

5. I can edit the product price and availability.

## Going Above and Beyond the Requirements

Given the time expectations of this exercise, we don't expect anyone to submit anything super fancy, but if you find yourself with extra time, any extra credit item(s) that showcase your unique strengths would be awesome! 🙌

## Submitting the Assignment

When you have finished the assignment send us the link to the repo (or a zip file) and we'll review your code within 1-2 days.

Thank you and good luck! 🙏
